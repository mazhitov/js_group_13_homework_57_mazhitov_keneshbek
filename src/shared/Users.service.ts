import { User } from './User.model';

export class UsersService {
  users: User[] = [
    new User('admin', 'admin@mail.ru', false,'admin'),
  ];

  roleArray= ['user', 'editor', 'admin'];
}
