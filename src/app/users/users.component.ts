import { Component, EventEmitter, Output } from '@angular/core';
import { UsersService } from '../../shared/Users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent  {
  @Output() changeUserProfile = new EventEmitter<number>();

  constructor(public usersService: UsersService) { }


  onChangeUser(id: number) {
    this.changeUserProfile.emit(id);
  }
}
