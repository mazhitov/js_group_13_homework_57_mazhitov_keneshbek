import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../shared/User.model';
import { UsersService } from '../../../shared/Users.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent  {
  @Input() user!: User;
  @Input() id!: number;
  @Output() changeUser = new EventEmitter<number>();
  constructor(public userService: UsersService) {}

  onDeleteUser() {
    this.userService.users.splice(this.id,1);
  }

  onChangeUser() {
    this.changeUser.emit(this.id);
  }
}
