import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  id = -1;
  check = false;

  onChangeUserProfile(id: number) {
    this.check = true;
    this.id = id;
  }
}
