import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { UsersService } from '../../shared/Users.service';
import { User } from '../../shared/User.model';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent {
  @ViewChild('userNameInput') userName!: ElementRef;
  @ViewChild('emailInput') emailAddress!: ElementRef;
  @ViewChild('activeCheckInput') activeCheck!: ElementRef;
  @ViewChild('selectedRole') roleValue!: ElementRef;
  @Input() checkUpdateUserProfile!:boolean;
  @Input() UserIndex!: number;

  constructor(public usersService: UsersService) {}


  onAdduser() {
    const name = this.userName.nativeElement.value;
    const email = this.emailAddress.nativeElement.value;
    const active = this.activeCheck.nativeElement.checked;
    const role = this.roleValue.nativeElement.value;
    const newUser = new User(name, email, active, role);
    console.log(this.checkUpdateUserProfile);
    if(this.checkUpdateUserProfile) {
      this.usersService.users[this.UserIndex] = newUser;
    } else {
      this.usersService.users.push(newUser);
    }
    this.checkUpdateUserProfile = false;
    this.reset();
  }

  reset() {
    this.userName.nativeElement.value = '';
    this.emailAddress.nativeElement.value = '';
    this.activeCheck.nativeElement.checked = false;
    this.roleValue.nativeElement.value = 'Choose role';
  }
  btnClassName() {
    return {
      'btn': true,
      'btn-primary': !this.checkUpdateUserProfile,
      'btn-success': this.checkUpdateUserProfile
    }
  }
  updateUserProfile() {
    if (this.checkUpdateUserProfile) {
      const user = this.usersService.users[this.UserIndex];
      this.userName.nativeElement.value = user.name;
      this.userName.nativeElement.value = user.name;
      this.emailAddress.nativeElement.value = user.email;
      this.activeCheck.nativeElement.checked = user.active;
      this.roleValue.nativeElement.value = user.role;
      return 'Save';
    }
    return 'Add';
  }
}
